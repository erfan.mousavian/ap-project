package cli;

import main.ConsoleColors;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Navigator {
    private static String[] colors = {ConsoleColors.RED_BACKGROUND,
                                        ConsoleColors.BLUE_BACKGROUND, ConsoleColors.CYAN_BACKGROUND};
    public static Stack<String> navigator;
    public static Stack<List<String>> clHelp;
    private static Navigator sample_instance = null;
    private Navigator() {
        navigator = new Stack<>();
        clHelp = new Stack<>();
    }
    public static Navigator getInstance() {
        if (sample_instance == null)
            sample_instance = new Navigator();
        return sample_instance;
    }
    public void pushNavigator(String s) {
        navigator.push(s);
    }
    public void popNavigator() {
        navigator.pop();
    }
    public void pushClHelp(List<String> l) {
        clHelp.push (l);
    }
    public void popClHelp() {
        clHelp.pop();
    }
    public void printNavigator() {
        for (int i = 0; i < navigator.size(); i++) {
            System.out.print(ConsoleColors.BLACK + colors[i % 3] + navigator.get(i) + ConsoleColors.RESET);
        }
        System.out.println();
    }
    public static void clear() {
        while (clHelp.size() > 0)
            clHelp.pop();
        while (navigator.size() > 0)
            navigator.pop();
    }
    public static void printHelps() {
        List <List <String>> l = new ArrayList<>();
        for (List <String> it : clHelp)
            l.add(it);
        System.out.println(ConsoleColors.YELLOW + "You can use these commands:" + ConsoleColors.RESET);
        //Writer.showTable(l);
    }
}
