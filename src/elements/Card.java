package elements;

import javafx.scene.image.Image;

public abstract class Card implements Comparable<Card> {
    public enum Type {
        FRUIT, SPELL, WEAPON;
    }

    public enum Rarity {
        COMMON, RARE, EPIC, LEGENDARY;
    }

    private String name;
    private String cardClass;
    private Type type;
    private Rarity rarity;
    private long mana;
    private long cost;
    private String ownerID;
    private String description;

    public Card(String name, String cardClass, String type, String rarity, long mana, long cost, String ownerID, String description) {
        this.name = name;
        this.cardClass = cardClass;
        this.type = Type.valueOf(type);
        this.rarity = Rarity.valueOf(rarity);
        this.mana = mana;
        this.cost = cost;
        this.ownerID = ownerID;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getCardClass() {
        return cardClass;
    }

    public Type getType() {
        return type;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public long getMana() {
        return mana;
    }

    public long getCost() {
        return cost;
    }

    public String getDescription() {
        return this.description;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCardClass(String cardClass) {
        this.cardClass = cardClass;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public void setMana(long mana) {
        this.mana = mana;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    @Override
    public int compareTo(Card rh) {
        return this.name.compareTo(rh.getName());
    }
}
