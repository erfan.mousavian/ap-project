package elements;

import javafx.scene.image.Image;
import main.InitialLoading;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CardReader {
    public static Card createCard(String cardName, String ownerID) { // create A card
        String name;
        String cardClass;
        String type;
        String rarity;
        long mana;
        long cost;
        String description;

        try (FileReader f = new FileReader(("src"+File.separator+"elements"+File.separator+"data"+File.separator+"cards"+File.separator+cardName+".json"))) {
            JSONParser parser = new JSONParser();
            JSONObject card = (JSONObject) parser.parse(f);
            name = (String)card.get("name");
            cardClass = (String)card.get("class");
            type = (String)card.get("type");
            rarity = (String)card.get("rarity");
            mana = (long)card.get("mana");
            description = (String)card.get("description");
            cost = (long)card.get("cost");

            if (type.equals("FRUIT")) {
                long attack = (long)card.get("attack");
                long HP = (long)card.get("HP");
                return new Fruit(name, cardClass, type, rarity, mana, cost, ownerID, description, attack, HP);
            }
            else if (type.equals("SPELL")) {
                return new Spell(name, cardClass, type, rarity, mana, cost, ownerID, description);
            }
            else if (type.equals("WEAPON")) {
                long attack = (long)card.get("attack");
                long durability = (long)card.get("durability");
                return new Weapon(name, cardClass, type, rarity, mana, cost, ownerID, description, attack, durability);
            }
            else {
                return null;
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Hero createHero(String cardName, ArrayList<Card> deck) { // create A card
        String name;
        long hp;
        try (FileReader f = new FileReader(("src"+File.separator+"elements"+File.separator+"data"+File.separator+"heros"+File.separator+cardName+".json"))) {
            JSONParser parser = new JSONParser();
            JSONObject card = (JSONObject) parser.parse(f);
            name = (String)card.get("name");
            hp = (long)card.get("HP");
            return new Hero(name, hp, deck);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
