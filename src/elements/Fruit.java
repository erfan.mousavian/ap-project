package elements;

import javafx.scene.image.Image;

public class Fruit extends Card {
    private long attack;
    private long hp;
    public Fruit (String name, String cardClass, String type, String rarity, long mana, long cost, String ownerID, String description, long attack, long hp) {
        super(name, cardClass, type, rarity, mana, cost, ownerID, description);
        this.attack = attack;
        this.hp = hp;
    }

    public long getAttack() {
        return attack;
    }

    public long getHP() {
        return hp;
    }
}