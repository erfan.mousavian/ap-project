package elements;

import main.ConsoleColors;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

public class Hero {
    private String name;
    private long hp;
    private ArrayList <Card> deck;

    public Hero (String name, long hp, ArrayList <Card> deck) {
        this.name = name;
        this.hp = hp;
        this.deck = deck;
    }

    public void showDeck () {
        HashMap<String, Integer> mp = new HashMap<>();
        for (Card i: deck) {
            if (mp.containsKey(i.getName())) {
                mp.remove(i.getName());
                mp.put(i.getName(), 2);
            }
            else {
                mp.put(i.getName(), 1);
            }
        }
        System.out.println(ConsoleColors.YELLOW_BOLD + "Your Hero's Deck:" + ConsoleColors.RESET);
//        try {
//            Writer.showTable(mp);
//        } catch (FileNotFoundException ignored) {
//
//        }
    }

    public void setName (String name) {
        this.name = name;
    }

    public JSONObject toJsonObject() {
        JSONObject ret = new JSONObject();
        ArrayList<String> deckCards = new ArrayList<>();
        for (Card i: deck)
            deckCards.add (i.getName());
        ret.put("name", this.name);
        ret.put("deck", deckCards);
        return ret;
    }
    public String getName () {
        return this.name;
    }

    public long getHp() {
        return hp;
    }

    public ArrayList<Card> getDeck() {
        return this.deck;
    }
}
