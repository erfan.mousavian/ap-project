package elements;

import javafx.scene.image.Image;

public class Spell extends Card {
    public Spell(String name, String cardClass, String type, String rarity, long mana, long cost, String ownerID, String description) {
        super(name, cardClass, type, rarity, mana, cost, ownerID, description);
    }
}
