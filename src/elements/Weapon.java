package elements;

import javafx.scene.image.Image;

public class Weapon extends Card {
    private long attack;
    private long durability;
    public Weapon (String name, String cardClass, String type, String rarity, long mana, long cost, String ownerID, String description, long attack, long durability) {
        super(name, cardClass, type, rarity, mana, cost, ownerID, description);
        this.attack = attack;
        this.durability = durability;
    }

    public long getAttack() {
        return attack;
    }

    public long getDurability() {
        return durability;
    }
}
