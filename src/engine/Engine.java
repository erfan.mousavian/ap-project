package engine;

import player.Player;

import java.io.File;
import java.util.ArrayList;

public class Engine {
    private static ArrayList<Player> players;
    private static Engine sample_instance = null;
    private Engine() {
        players = new ArrayList<>();
    }
    public static Engine getInstance() {
        if (sample_instance == null)
            sample_instance = new Engine();
        return sample_instance;
    }
    public void addPlayer(Player p) {
        players.add (p);
    }
    public Player getPlayer(int id) {
        return players.get(id);
    }
    public void logOutPlayer(Player p) {
        players.remove(p);
        p.savePlayer();
    }
    public void removePlayer(Player p) {
        players.remove(p);
        File f = new File ("src"+File.separator+"player"+File.separator+"data"+File.separator+p.getUsername()+".json");
        f.delete();
    }
}
