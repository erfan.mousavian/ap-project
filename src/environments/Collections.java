package environments;

import cli.Navigator;
import engine.Engine;
import exeptions.ExitException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import logger.LogWriter;
import main.ConsoleColors;
import main.Blackbox;
import main.Hearthstone;
import org.json.simple.parser.ParseException;
import player.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Collections {
    private static Collections singleton = null;
    private static Scene scene;
    private Player p;

    private Collections() {
        p = Engine.getInstance().getPlayer(0);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("collections.fxml"));
        Parent root = null;
        try {
            root = (Parent) loader.load();
        } catch (Exception e) {}

        scene = new Scene(root, 1280, 720);
    }

    public void show() {
        Hearthstone.getStage().setScene(scene);
    }

    public static Collections getInstance() {
        if (singleton == null)
            singleton = new Collections();
        return singleton;
    }

//    private void cli() throws ExitException, InterruptedException, IOException, ParseException {
//        Scanner sc = Hearthstone.sc;
//        while (true) {
//            Navigator.getInstance().printNavigator();
//            String nxt = sc.nextLine();
//            nxt = Blackbox.getInstance().makeStringGreatAgain(nxt);
//            String[] splited = nxt.split(" ");
//            if (nxt.equalsIgnoreCase("cd ../")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "navigate " + Blackbox.getInstance().getCurrentTime() + " back to parent directory");
//                return;
//            }
//            else if (nxt.equalsIgnoreCase("exit")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "logout " + Blackbox.getInstance().getCurrentTime());
//                Engine.getInstance().logOutPlayer(Engine.getInstance().getPlayer(0));
//                System.out.println(ConsoleColors.GREEN + "You have successfully logged out." + ConsoleColors.RESET);
//                Thread.sleep(1000);
//                throw new ExitException("Logout from account");
//            }
//            else if (nxt.equalsIgnoreCase("exit -a")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "terminate program " + Blackbox.getInstance().getCurrentTime());
//                Engine.getInstance().logOutPlayer(Engine.getInstance().getPlayer(0));
//                System.out.println(ConsoleColors.GREEN + "You have successfully logged out." + ConsoleColors.RESET);
//                Thread.sleep(1000);
//                System.exit(0);
//                return;
//            }
//            else if (nxt.equalsIgnoreCase("rm -p")) {
//                System.out.print ("please enter password: ");
//                String pass = sc.nextLine();
//                pass = Blackbox.getInstance().getHash(pass);
//                Player p = Engine.getInstance().getPlayer(0);
//                if (p.getPassword().equals(pass)) {
//                    LogWriter.appendLog(Engine.getInstance().getPlayer(0), "==============\n\nremoved at " + Blackbox.getInstance().getCurrentTime());
//                    Engine.getInstance().removePlayer(p);
//                    System.out.println(ConsoleColors.GREEN + "Your account has been removed!" + ConsoleColors.RESET);
//                    Thread.sleep(1000);
//                    throw new ExitException("Account has been removed!");
//                }
//                else
//                    System.out.println(ConsoleColors.RED + "Wrong Password!" + ConsoleColors.RESET);
//            }
//            else if (nxt.equalsIgnoreCase("ls -a -heros")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " player heros");
//                p.showHeros();
//            }
//            else if (nxt.equalsIgnoreCase("ls -m -heros")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " selected hero");
//                p.showSelectedHero();
//            }
//            else if (splited[0].equalsIgnoreCase("select")) {
//                if (splited.length > 1) {
//                    if (p.selectHero (splited[1])) {
//                        LogWriter.appendLog(Engine.getInstance().getPlayer(0), "select " + Blackbox.getInstance().getCurrentTime() + " hero:" + splited[1]);
//                        p.savePlayer();
//                        System.out.println(ConsoleColors.GREEN + "Done!" + ConsoleColors.RESET);
//                    }
//                    else {
//                        System.out.println(ConsoleColors.RED + "You dont have this Hero, try again." + ConsoleColors.RESET);
//                    }
//                }
//                else {
//                    System.out.println(ConsoleColors.RED + "Invalid Command, try again." + ConsoleColors.RESET);
//                }
//            }
//            else if (nxt.equalsIgnoreCase("ls -a -cards")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " unlocked cards for hero");
//                p.cardsForHero();
//            }
//            else if (nxt.equalsIgnoreCase("ls -m -cards")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " hero deck");
//                p.getSelectedHero().showDeck();
//            }
//            else if (nxt.equalsIgnoreCase("ls -n -cards")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " cards can be added to deck");
//                p.canBeAddedToDeck();
//            }
//            else if (splited[0].equalsIgnoreCase("add")) {
//                if (splited.length > 1 && p.addCardToDeck (splited[1])) {
//                    LogWriter.appendLog(Engine.getInstance().getPlayer(0), "add " + Blackbox.getInstance().getCurrentTime() + " add " + splited[1] + " to deck");
//                    p.savePlayer();
//                    System.out.println(ConsoleColors.GREEN + "Done!" + ConsoleColors.RESET);
//                }
//                else
//                    System.out.println(ConsoleColors.RED + "You cant add this card into your deck, try again." + ConsoleColors.RESET);
//            }
//            else if (splited[0].equalsIgnoreCase("remove")) {
//                if (splited.length > 1 && p.removeFromDeck (splited[1])) {
//                    LogWriter.appendLog(Engine.getInstance().getPlayer(0), "remove " + Blackbox.getInstance().getCurrentTime() + " remove " + splited[1] + " from deck");
//                    p.savePlayer();
//                    System.out.println(ConsoleColors.GREEN + "Done!" + ConsoleColors.RESET);
//                }
//                else
//                    System.out.println(ConsoleColors.RED + "You cant remove this card from your deck, try again." + ConsoleColors.RESET);
//            }
//            else if (nxt.equalsIgnoreCase("hearthstone --help")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " get valid commands");
//                Navigator.printHelps();
//            }
//            else {
//                System.out.println(ConsoleColors.RED + "Invalid Command, try again." + ConsoleColors.RESET);
//            }
//        }
//    }
}
