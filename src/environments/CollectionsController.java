package environments;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import elements.Card;
import elements.CardReader;
import engine.Engine;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import main.InitialLoading;
import player.Player;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

public class CollectionsController implements Initializable {
    @FXML
    private GridPane gridPane;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private JFXTextField searchField;

    @FXML
    private ChoiceBox<String> cardClass;

    private Player p;

    public void refresh() {
        double width = (0.55) * (double) 400;
        double height = (0.55) * (double) 543;

        gridPane.getChildren().clear();

        Map<String, Integer> ownedCards = p.ownedCards();
        Map<String, Integer> unlockedCards = p.unlockedCards();

        int cnt = 0;

        for (Map.Entry<String, Integer> entry: ownedCards.entrySet()) {
            if (!entry.getKey().toLowerCase().contains(searchField.getText().toLowerCase()))
                continue;
            Card card = CardReader.createCard(entry.getKey(), "0");
            if (!card.getCardClass().equals(cardClass.getValue()) && !cardClass.getValue().equals("ALL"))
                continue;
            ImageView imageView = new ImageView(InitialLoading.getInstance().getImage(entry.getKey()));
            imageView.setFitWidth(width);
            imageView.setFitHeight(height);
            gridPane.add(imageView, cnt % 5, cnt / 5);
            cnt++;
        }

        for (Map.Entry<String, Integer> entry: unlockedCards.entrySet()) {
            if (entry.getValue() != InitialLoading.getInstance().getMaxPerCard())
                continue;
            if (!entry.getKey().toLowerCase().contains(searchField.getText().toLowerCase()))
                continue;
            Card card = CardReader.createCard(entry.getKey(), "0");
            if (!card.getCardClass().equals(cardClass.getValue()) && !cardClass.getValue().equals("ALL"))
                continue;
            ImageView imageView = new ImageView(InitialLoading.getInstance().getImage(entry.getKey()));
            imageView.setFitWidth(width);
            imageView.setFitHeight(height);
            ColorAdjust monochrome = new ColorAdjust();
            monochrome.setSaturation(-1);
            imageView.setEffect(monochrome);
            gridPane.add(imageView, cnt % 5, cnt / 5);
            cnt++;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        p = Engine.getInstance().getPlayer(0);

        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        searchField.textProperty().addListener((observableValue, s, t1) -> refresh());

        cardClass.getItems().add("ALL");
        cardClass.getItems().add("neutral");
        cardClass.getItems().addAll(InitialLoading.getInstance().getHeros());
        cardClass.setValue("ALL");
        cardClass.getSelectionModel().selectedItemProperty().addListener((observableValue, s, t1) -> refresh());

        refresh();
    }
}
