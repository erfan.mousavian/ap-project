package environments;

import cli.Navigator;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import main.Hearthstone;

public class Menu {
    private static Menu singleton = null;
    private static Scene scene;
    public static Label test;

    public static Menu getInstance() {
        if (singleton == null)
            singleton = new Menu();
        return singleton;
    }

    private Menu() {
        test = new Label("test");

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("menu.fxml"));
        } catch (Exception ignored) {
            System.out.println("HI");
        }

        scene = new Scene(root, 1280, 720);
    }

    public void show() {
        Hearthstone.getStage().setScene(scene);
    }

//    public void cli () {
//        List <String> l;
//        Scanner sc = Hearthstone.sc;
//        while (true) {
//            Navigator.getInstance().printNavigator();
//            String nxt = sc.nextLine();
//            nxt = Blackbox.getInstance().makeStringGreatAgain(nxt);
//            if (nxt.equalsIgnoreCase("exit")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "logout " + Blackbox.getInstance().getCurrentTime());
//                Engine.getInstance().logOutPlayer(Engine.getInstance().getPlayer(0));
//                System.out.println(ConsoleColors.GREEN + "You have successfully logged out." + ConsoleColors.RESET);
//                Thread.sleep(1000);
//                throw new ExitException("Logout from account");
//            }
//            else if (nxt.equalsIgnoreCase("exit -a")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "terminate program " + Blackbox.getInstance().getCurrentTime());
//                Engine.getInstance().logOutPlayer(Engine.getInstance().getPlayer(0));
//                System.out.println(ConsoleColors.GREEN + "You have successfully logged out." + ConsoleColors.RESET);
//                Thread.sleep(1000);
//                Navigator.clear();
//                System.exit(0);
//                return;
//            }
//            else if (nxt.equalsIgnoreCase("rm -p")) {
//                System.out.print ("please enter password: ");
//                String pass = sc.nextLine();
//                pass = Blackbox.getInstance().getHash(pass);
//                Player p = Engine.getInstance().getPlayer(0);
//                if (p.getPassword().equals(pass)) {
//                    LogWriter.appendLog(Engine.getInstance().getPlayer(0), "==============\n\nremoved at " + Blackbox.getInstance().getCurrentTime());
//                    Engine.getInstance().removePlayer(p);
//                    System.out.println(ConsoleColors.GREEN + "Your account has been removed!" + ConsoleColors.RESET);
//                    Thread.sleep(1000);
//                    throw new ExitException("Account has been removed!");
//                }
//                else
//                    System.out.println(ConsoleColors.RED + "Wrong Password!" + ConsoleColors.RESET);
//            }
//            else if (nxt.equalsIgnoreCase("cd collections")) {
//                Navigator.getInstance().popClHelp();
//                Navigator.getInstance().popClHelp();
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "navigate " + Blackbox.getInstance().getCurrentTime() + " collections");
//                try {
//                    new Collections();
//                } catch (ExitException e) {
//                    return;
//                }
//                l = new ArrayList<>();
//                l.add("cd collections");
//                l.add("move to collections");
//                Navigator.getInstance().pushClHelp(l);
//                l = new ArrayList<>();
//                l.add("cd store");
//                l.add("move to store");
//                Navigator.getInstance().pushClHelp(l);
//            }
//            else if (nxt.equalsIgnoreCase("cd store")) {
//                Navigator.getInstance().popClHelp();
//                Navigator.getInstance().popClHelp();
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "navigate " + Blackbox.getInstance().getCurrentTime() + " store");
//                try {
//                    new Store();
//                } catch (ExitException e) {
//                    return;
//                }
//                l = new ArrayList<>();
//                l.add("cd collections");
//                l.add("move to collections");
//                Navigator.getInstance().pushClHelp(l);
//                l = new ArrayList<>();
//                l.add("cd store");
//                l.add("move to store");
//                Navigator.getInstance().pushClHelp(l);
//            }
//            else if (nxt.equalsIgnoreCase("hearthstone --help")) {
//                LogWriter.appendLog(Engine.getInstance().getPlayer(0), "show " + Blackbox.getInstance().getCurrentTime() + " get valid commands");
//                Navigator.printHelps();
//            }
//            else {
//                System.out.println(ConsoleColors.RED + "Invalid Command, try again." + ConsoleColors.RESET);
//            }
//        }
//    }
}
