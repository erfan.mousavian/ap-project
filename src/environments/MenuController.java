package environments;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class MenuController {
    @FXML
    void setStoreButton(ActionEvent event) {
        Store.getInstance().show();
    }

    @FXML
    void setCollectionsButton(ActionEvent event) {
        Collections.getInstance().show();
    }

}
