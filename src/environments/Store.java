package environments;

import cli.Navigator;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import engine.Engine;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import main.Controller;
import main.Hearthstone;
import player.Player;
import javafx.scene.layout.GridPane;
import java.util.Arrays;


public class Store {
    private static Store singleton;
    private static Scene scene;
    private Player p;

    public static Store getInstance() {
        if (singleton == null)
            singleton = new Store();
        return singleton;
    }

    private Store() {
        p = Engine.getInstance().getPlayer(0);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("store.fxml"));
        Parent root = null;
        try {
            root = (Parent) loader.load();
        } catch (Exception e) {}

        scene = new Scene(root, 1280, 720);
    }

    public void show() {
        Hearthstone.getStage().setScene(scene);
    }
}