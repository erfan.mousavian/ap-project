package environments;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXScrollPane;
import com.jfoenix.controls.JFXToggleButton;
import com.sun.prism.Image;
import elements.CardReader;
import engine.Engine;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import main.InitialLoading;
import player.Player;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

public class StoreController implements Initializable {
    @FXML
    private GridPane gridPane;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    void setBackButton(ActionEvent event) {
        Menu.getInstance().show();
    }

    @FXML
    private JFXToggleButton isOwned;

    @FXML
    private VBox cardPreview;

    @FXML
    private Label coinLabel;

    private Player p;

    public void refresh() {
        coinLabel.setText(Long.toString(p.getCoins()));

        drawPreview("Back");

        double width = (0.55) * (double) 400;
        double height = (0.55) * (double) 543;

        gridPane.getChildren().clear();

        Map <String, Integer> cards = isOwned.isSelected() ? p.ownedCards() : p.notOwnedCards();

        int cnt = 0;

        for (Map.Entry<String, Integer> entry: cards.entrySet()) {
            ImageView imageView = new ImageView(InitialLoading.getInstance().getImage(entry.getKey()));
            imageView.setFitWidth(width);
            imageView.setFitHeight(height);
            imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> drawPreview(entry.getKey()));
            gridPane.add(imageView, cnt % 4, cnt / 4);
            cnt++;
        }
    }

    public void drawPreview (String cardName) {
        double width = (1.2) * (double) 200;
        double height = (1.2) * (double) 276;

        cardPreview.getChildren().clear();

        ImageView imageView = new ImageView(InitialLoading.getInstance().getImage(cardName));
        imageView.setFitHeight(height);
        imageView.setFitWidth(width);
        cardPreview.getChildren().add (imageView);

        int cntValid = 0;

        Label label = new Label();
        if (cardName.equals("Back")) {
            label.setText("You dont selected any card");
        }
        else {
            cntValid = isOwned.isSelected() ? p.howMuchCanSell(cardName) : p.howMuchCanBuy(cardName);
            label.setText("You can " + (isOwned.isSelected() ? "sell " : "buy ") + cntValid + " of this card");
        }
        cardPreview.getChildren().add(label);

        Label price = new Label();
        if (!cardName.equals("Back")) {
            String val = Long.toString(CardReader.createCard(cardName, "0").getCost() / (isOwned.isSelected() ? 2 : 1));
            price.setText((isOwned.isSelected() ? "Sell " : "Buy ") + "price per each card: " + val);
            cardPreview.getChildren().add(price);
        }

        ChoiceBox <Integer> choiceBox = new ChoiceBox<>();
        for (int i = 1; i <= cntValid; i++)
            choiceBox.getItems().add (i);
        choiceBox.setValue(1);

        JFXButton btn = new JFXButton(isOwned.isSelected() ? "Sell" : "Buy");
        btn.setStyle("-fx-background-color:" + (isOwned.isSelected() ? "#db4f42" : "#3ebd82") + ";");
        btn.setTextFill(Color.WHITE);
        btn.setOnAction(e -> {
            if (isOwned.isSelected())
                p.sellCard(cardName, choiceBox.getValue());
            else
                p.buyCard(cardName, choiceBox.getValue());
            refresh();
        });

        if (cntValid == 0) {
            btn.setDisable(true);
            choiceBox.setDisable(true);
        }

        if (!cardName.equals("Back")) {
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER);
            hBox.setSpacing(10);
            hBox.getChildren().addAll(choiceBox, btn);
            cardPreview.getChildren().add(hBox);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        p = Engine.getInstance().getPlayer(0);

        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        isOwned.selectedProperty().addListener((observableValue, aBoolean, t1) -> {
            isOwned.setText(isOwned.isSelected() ? "Owned" : "Now Owned");
            refresh();
        });

        refresh();
    }
}
