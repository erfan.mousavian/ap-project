package exeptions;

public class ExitException extends Exception {
    public ExitException (String s) {
        super(s);
    }
}