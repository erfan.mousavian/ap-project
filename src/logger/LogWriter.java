package logger;

import main.Blackbox;
import player.Player;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogWriter {
    public static void createUser(String userName, String userID, String password) {
        File f = new File("src"+File.separator+"logger"+File.separator+"logs"+File.separator+userName+"-"+userID+".log");

        boolean created = false;

        try {
            created = f.createNewFile();
        } catch (Exception ignored) {};

        if (created) {
            try {
                FileWriter fw = new FileWriter(f);
                fw.append("username: ").append(userName);
                fw.append("\n");
                fw.append("password: ").append(password);
                fw.append("\n");
                fw.append("userID: ").append(userID);
                fw.append("\n");
                fw.append("created at: ").append(Blackbox.getInstance().getCurrentTime());
                fw.append("\n\n==============\n");
                fw.flush();
                fw.close();
            } catch (Exception e) {
                programLog("Cant create new account", userName + " " + userID);
                programLog(e.getMessage());
            }
        }
    }

    public static void appendLog (Player p, String s) {
        File f = new File("src"+File.separator+"logger"+File.separator+"logs"+File.separator+p.getUsername()+"-"+p.getId()+".log");

        try {
            FileWriter fw = new FileWriter(f, true);
            fw.append(s).append("\n");
            fw.flush();
            fw.close();
        } catch (Exception e) {
            programLog("cant append log", f.getPath() + " :\n " + p + "\n" + s);
        }
    }

    public static void programLog (String txt) {
        File f = new File("src" + File.separator + "logger" + File.separator + "logs" + File.separator + "programRun.log");
        try {
            FileWriter fw = new FileWriter(f, true);
            fw.append(txt).append("\n");
            fw.flush();
            fw.close();
        } catch (Exception ignored) {

        }
    }

    public static void programLog (String txt, String txt2) {
        File f = new File("src" + File.separator + "logger" + File.separator + "logs" + File.separator + "programLogs.log");
        try {
            FileWriter fw = new FileWriter(f, true);
            fw.append("### ").append(txt).append("\n");
            fw.append(txt2).append("\n");
            fw.flush();
            fw.close();
        } catch (Exception ignored) {

        }
    }
}
