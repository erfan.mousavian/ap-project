package main;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Blackbox {
    private static long P;
    private static int MOD;
    private static String pat;
    private static SimpleDateFormat formatter;
    private static Date date;
    private static Blackbox sample_instance = null;

    private Blackbox() {
        P = 337;
        MOD = 1000 * 1000 * 1000 + 9;
        pat = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz!$%^&";
        formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    }

    public static Blackbox getInstance() {
        if (sample_instance == null)
            sample_instance = new Blackbox();
        return sample_instance;
    }

    public String getHash (String s) {
        int z = s.length();
        for (int i = 0; i < 8 - z; i++)
            s += s.charAt(i % z);
        long ret = 0;
        String res = "";
        for (int i = 0; i < (int)s.length(); i++) {
            ret *= P;
            ret += s.charAt(i);
            ret %= MOD;
            res += pat.charAt((int)(ret % pat.length()));
        }
        return res;
    }

    public String getRandomString (int len) {
        String res = "";
        Random r = new Random();
        for (int i = 0; i < len; i++)
            res += pat.charAt(r.nextInt(pat.length()));
        return res;
    }

    public int LCS (String s, String t) {
        s = s.toLowerCase();
        t = t.toLowerCase();
        int[][] dp = new int[s.length() + 1][t.length() + 1];
        int mx = 0;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 0; j <= t.length(); j++) {
                if (i > 0)
                    dp[i][j] = dp[i - 1][j];
                if (j > 0)
                    dp[i][j] = Math.max (dp[i][j - 1], dp[i][j]);
                if (i > 0 && j > 0 && s.charAt(i - 1) == t.charAt(j - 1))
                    dp[i][j] = Math.max (dp[i][j], dp[i - 1][j - 1] + 1);
                mx = Math.max (dp[i][j], mx);
            }
        }
        return mx;
    }

    public String getCurrentTime() {
        date = new Date();
        return formatter.format(date);
    }

    public String makeStringGreatAgain(String pat) { // HA HA :) Good names :)
        String s = "";
        for (int i = 0; i < pat.length(); i++) {
            if (i + 1 < pat.length() && pat.charAt(i) == ' ' && pat.charAt(i + 1) == ' ')
                continue;
            if (i + 1 == pat.length() && pat.charAt(i) == ' ')
                continue;
            if (s.length() == 0 && pat.charAt(i) == ' ')
                continue;
            s += pat.charAt(i);
        }
        return s;
    }
}
