package main;

import com.jfoenix.controls.JFXTextField;
import exeptions.MyException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import player.signUp;
import player.signIn;

public class Controller {
    @FXML
    private JFXTextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Label warningLabel;

    @FXML
    private Label successLabel;

    @FXML
    private Button closeButton;

    @FXML
    void setLoginButton(ActionEvent event) {
        try {
            signIn.getInstance().logIn(username.getText(), password.getText());
        } catch (MyException e) {
            successLabel.setVisible(false);
            warningLabel.setText(e.getMessage());
            warningLabel.setVisible(true);
            closeButton.setVisible(true);
        }
    }

    @FXML
    void setRegisterButton(ActionEvent event) {
        try {
            signUp.getInstance().createUser(username.getText(), password.getText());
            warningLabel.setVisible(false);
            successLabel.setText("You have registered successfully");
            successLabel.setVisible(true);
            closeButton.setVisible(true);
        } catch (MyException e) {
            successLabel.setVisible(false);
            warningLabel.setText(e.getMessage());
            warningLabel.setVisible(true);
            closeButton.setVisible(true);
        }
    }

    @FXML
    void setCloseButton(ActionEvent event) {
        warningLabel.setVisible(false);
        successLabel.setVisible(false);
        closeButton.setVisible(false);
    }

}