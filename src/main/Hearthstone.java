package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Scanner;

public class Hearthstone extends Application {
    public static Scanner sc = new Scanner(System.in);
    private static Stage stage;
    private static Scene scene;

    private static void preProcess() {
        InitialLoading.getInstance();
        sc = new Scanner(System.in);
    }

    public static void main(String[] args) {
        preProcess();
        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("initialPage.fxml"));
        stage = primaryStage;
        stage.setTitle("Hearthstone");
        scene = new Scene(root, 1280, 720);
        stage.setScene(scene);
        stage.show();
    }
}
// javac -Xlint:unchecked -cp src/:src/libraries/json-simple-1.1.1.jar:. src/main/Hearthstone.java
// java -cp src/:src/libraries/json-simple-1.1.1.jar:. main/Hearthstone

// --module-path /home/erfan/Documents/javafx-sdk-11.0.2/lib --add-modules javafx.controls,javafx.fxml --add-opens javafx.base/com.sun.javafx.runtime=ALL-UNNAMED --add-opens javafx.controls/com.sun.javafx.scene.control.behavior=ALL-UNNAMED --add-opens javafx.controls/com.sun.javafx.scene.control=ALL-UNNAMED --add-opens javafx.base/com.sun.javafx.binding=ALL-UNNAMED --add-opens javafx.base/com.sun.javafx.event=ALL-UNNAMED --add-opens javafx.graphics/com.sun.javafx.stage=ALL-UNNAMED