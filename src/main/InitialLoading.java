package main;

import elements.Card;
import elements.CardReader;
import javafx.scene.image.Image;
import logger.LogWriter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.*;

public class InitialLoading {
    private static InitialLoading single_instance = null;
    private static long playerCoins = 0;
    private static long heroDeckCapacity = 0;
    private static long maxPerCard = 0;
    private static ArrayList<String> heros;
    private static ArrayList<String> cards;
    private static ArrayList<Card> gameCards;
    private static Map<String, Image> cardImages;
    private static Random r;

    private InitialLoading () {
        //Make directories
        File f = new File ("src"+File.separator+"logger"+File.separator+"logs");
        f.mkdir();

        f = new File ("src"+File.separator+"player"+File.separator+"data");
        f.mkdir();

        f = new File ("src"+File.separator+"logger"+File.separator+"logs"+File.separator+"programLogs.log");
        try {
            f.createNewFile();
        } catch (Exception ignored) {}

        //Initialize Random
        r = new Random();

        //Read config
        f = new File ("src"+File.separator+"resources"+File.separator+"config.json");
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse (new FileReader(f));
            playerCoins = (long) obj.get("PLAYER_COINS");
            heroDeckCapacity = (long) obj.get("HERO_DECK_CAPACITY");
            maxPerCard = (long) obj.get ("MAX_PER_CARD");
        } catch (Exception e) {
            LogWriter.programLog("Initialize ");
        }

        // Read Game Heros
        f = new File("src"+File.separator+"elements"+File.separator+"data"+File.separator+"heros");
        heros = new ArrayList<>();
        for (int i = 0; i < Objects.requireNonNull(f.listFiles()).length; i++)
            heros.add(withoutFormat(Objects.requireNonNull(f.listFiles())[i].getName()));

        // Read Game Cards
        f = new File("src"+File.separator+"elements"+File.separator+"data"+File.separator+"cards");
        cards = new ArrayList<>();
        gameCards = new ArrayList<>();
        for (int i = 0; i < Objects.requireNonNull(f.listFiles()).length; i++) {
            String name = withoutFormat(Objects.requireNonNull(f.listFiles())[i].getName());
            if (name.equals("Back"))
                continue;
            cards.add(name);
            gameCards.add (CardReader.createCard(name, "0"));
        }

        // Load Card images
        f = new File("src"+File.separator+"resources"+File.separator+"cards");
        cardImages = new HashMap<>();
        for (int i = 0; i < Objects.requireNonNull(f.listFiles()).length; i++) {
            try {
                String name = withoutFormat(Objects.requireNonNull(f.listFiles())[i].getName());
                Image img = new Image(new FileInputStream("src"+File.separator+"resources"+File.separator+"cards"+File.separator+name+".png"));
                cardImages.put(name, img);
            } catch (Exception ignored) {}
        }
    }

    public static InitialLoading getInstance() {
        if (single_instance == null)
            single_instance = new InitialLoading();
        return single_instance;
    }

    public String withoutFormat (String s) {
        String ret = "";
        int id = 0;
        while (id < s.length() && s.charAt(id) != '.')
            ret += s.charAt(id++);
        return ret;
    }

    public ArrayList<String> getCards () {
        return cards;
    }

    public Image getImage (String cardName) {
        if (cardImages.containsKey(cardName))
            return cardImages.get(cardName);
        return null;
    }

    public long getPlayerCoins () {
        return playerCoins;
    }

    public ArrayList<String> getHeros() {
        return heros;
    }

    public String getRandomHero() {
        return heros.get(r.nextInt(heros.size()));
    }

    public long getHeroDeckCapacity() {
        return heroDeckCapacity;
    }

    public long getMaxPerCard() {
        return maxPerCard;
    }

    public ArrayList<Card> getGameCards() {
        return gameCards;
    }
}