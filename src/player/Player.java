package player;

import java.io.*;
import java.util.*;

import elements.Card;
import elements.CardReader;
import elements.Hero;
import main.Blackbox;
import main.ConsoleColors;
import main.InitialLoading;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Player {
    private String id;
    private String password;
    private long coins;
    private String username;
    private List<Card> cards;
    private List<Hero> heros;
    private Hero selectedHero;

    public Player(String username) { // login
        try (FileReader reader = new FileReader("src"+File.separator+"player"+File.separator+ "data" + File.separator + username + ".json")) {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(reader);
            id = (String) obj.get("id");
            password = (String) obj.get("password");
            coins = (long) obj.get("coins");
            this.username = (String) obj.get("username");
            // Read Cards
            JSONArray playerCards = (JSONArray) obj.get("cards");
            cards = new ArrayList<>();
            for (int i = 0; i < playerCards.size(); i++)
                cards.add(CardReader.createCard(playerCards.get(i).toString(), id));
            // Read Heros
            JSONArray playerHeros = (JSONArray) obj.get("heros");
            heros = new ArrayList<>();
            for (int i = 0; i < playerHeros.size(); i++) {
                ArrayList<Card> heroDeck = new ArrayList<>();
                JSONObject nowHero = (JSONObject) playerHeros.get(i);
                JSONArray stringOfDeck = (JSONArray) nowHero.get("deck");
                boolean[] is = new boolean[cards.size()];
                for (int j = 0; j < stringOfDeck.size(); j++) {
                    String name = stringOfDeck.get(j).toString();
                    for (int k = 0; k < cards.size(); k++) {
                        if (is[k])
                            continue;
                        if (cards.get(k).getName().equals(name)) {
                            heroDeck.add(cards.get(k));
                            is[k] = true;
                            break;
                        }
                    }
                }
                heros.add(CardReader.createHero((String) nowHero.get("name"), heroDeck));
            }
            for (Hero i : heros) {
                if (i.getName().equals((String) obj.get("selectedHero"))) {
                    selectedHero = i;
                    break;
                }
            }
        } catch (Exception ignored) {
        }
    }

    public JSONObject toJsonObject() {
        JSONObject ret = new JSONObject();
        List<JSONObject> herosJson = new ArrayList<>();
        for (Hero i : this.heros)
            herosJson.add(i.toJsonObject());
        ret.put("heros", herosJson);
        ret.put("password", this.password);
        ret.put("selectedHero", this.selectedHero.getName());
        List<String> cardsJson = new ArrayList<>();
        for (Card i : this.cards)
            cardsJson.add(i.getName());
        ret.put("cards", cardsJson);
        ret.put("coins", this.coins);
        ret.put("id", this.id);
        ret.put("username", this.username);
        return ret;
    }

    public void savePlayer() {
        try {
            PrintWriter pw = new PrintWriter("src"+File.separator+"player"+File.separator+"data"+File.separator + this.username + ".json");
            pw.write(this.toJsonObject().toJSONString());
            pw.close();
        } catch (FileNotFoundException ignored) {}
    }

    public Map<String, Integer> ownedCards () {
        Map<String, Integer> mp = new HashMap<>();
        for (Card i: cards) {
            if (mp.containsKey(i.getName())) {
                int c = mp.get(i.getName()) + 1;
                mp.remove(i.getName());
                mp.put(i.getName(), c);
            }
            else
                mp.put(i.getName(), 1);
        }
        return mp;
    }

    public Map<String, Integer> notOwnedCards() {
        HashMap<String, Integer> mp = new HashMap<>();
        for (Card i: InitialLoading.getInstance().getGameCards()) {
            if (i.getCardClass().equals("neutral"))
                mp.put(i.getName(), (int)InitialLoading.getInstance().getMaxPerCard());
            else {
                for (Hero j: heros) {
                    if (j.getName().equals(i.getCardClass())) {
                        mp.put (i.getName(), (int)InitialLoading.getInstance().getMaxPerCard());
                        break;
                    }
                }
            }
        }
        for (Card i: cards) {
            int c = mp.get(i.getName());
            mp.remove(i.getName());
            if (c > 1)
                mp.put(i.getName(), c - 1);
        }
        return mp;
    }

    public Map<String, Integer> unlockedCards() {
        HashMap<String, Integer> mp = new HashMap<>();
        for (String i: InitialLoading.getInstance().getCards())
            mp.put(i, (int) InitialLoading.getInstance().getMaxPerCard());
        for (Card i: cards) {
            int c = mp.get(i.getName());
            mp.remove(i.getName());
            if (c > 1)
                mp.put(i.getName(), c - 1);
        }
        return mp;
    }

    public void showHeros () {
        List <List <String>> list = new ArrayList<>();
        List <String> title = new ArrayList<>();
        title.add("Name");
        title.add("HP");
        list.add(title);
        for (Hero i: heros) {
            List <String> nowHero = new ArrayList<>();
            nowHero.add (i.getName());
            nowHero.add (Long.toString(i.getHp()));
            list.add (nowHero);
        }
//        Writer.showTable(list);
    }

    public void showSelectedHero () {
        System.out.print(ConsoleColors.YELLOW_BOLD + "Your Selected Hero: " + ConsoleColors.RESET);
        System.out.println(selectedHero.getName());
    }

    public boolean selectHero (String s) {
        int mx = 0, id = 0;
        for (int i = 0; i < heros.size(); i++) {
            int c = Blackbox.getInstance().LCS(s, heros.get(i).getName());
            if (c > mx) {
                mx = c;
                id = i;
            }
        }
        if (mx > 2) {
            this.selectedHero = heros.get(id);
            return true;
        }
        return false;
    }

    public void cardsForHero() {
        HashMap<String, Integer> mp = new HashMap<>();
        for (Card i: cards) {
            if (i.getCardClass().equals(selectedHero.getName()) || i.getCardClass().equals("neutral")) {
                if (mp.containsKey(i.getName())) {
                    int c = mp.get(i.getName());
                    mp.remove(i.getName());
                    mp.put(i.getName(), c + 1);
                }
                else {
                    mp.put(i.getName(), 1);
                }
            }
        }
        System.out.println(ConsoleColors.YELLOW_BOLD + "All cards for Selected Hero:" + ConsoleColors.RESET);
//        try {
//            Writer.showTable(mp);
//        } catch (FileNotFoundException ignored) {
//
//        }
    }

    public void canBeAddedToDeck() {
        HashMap<String, Integer> mp = new HashMap<>();
        for (Card i: cards) {
            if (i.getCardClass().equals(selectedHero.getName()) || i.getCardClass().equals("neutral")) {
                if (mp.containsKey(i.getName())) {
                    int c = mp.get(i.getName());
                    mp.remove(i.getName());
                    mp.put(i.getName(), c + 1);
                }
                else {
                    mp.put(i.getName(), 1);
                }
            }
        }
        for (Card i: selectedHero.getDeck()) {
            int cnt = mp.get(i.getName());
            mp.remove(i.getName());
            cnt--;
            if (cnt > 0)
                mp.put (i.getName(), cnt);
        }
        int mx = 0;
        try {
            mx = (int) InitialLoading.getInstance().getHeroDeckCapacity();
        } catch (Exception ignored) {

        }
        if (selectedHero.getDeck().size() == mx) {
            System.out.println(ConsoleColors.RED + "Your Deck is full, you cant add card to it" + ConsoleColors.RESET);
            mp.clear();
            return;
        }
        System.out.println(ConsoleColors.YELLOW_BOLD + "You can select these cards:" + ConsoleColors.RESET);
//        try {
//            Writer.showTable(mp);
//        } catch (FileNotFoundException ignored) {
//
//        }
    }

    public boolean addCardToDeck(String cardName) {
        HashMap<String, Integer> mp = new HashMap<>();
        for (Card i: cards) {
            if (i.getCardClass().equals(selectedHero.getName()) || i.getCardClass().equals("neutral")) {
                if (mp.containsKey(i.getName())) {
                    int c = mp.get(i.getName());
                    mp.remove(i.getName());
                    mp.put(i.getName(), c + 1);
                }
                else {
                    mp.put(i.getName(), 1);
                }
            }
        }
        for (Card i: selectedHero.getDeck()) {
            int cnt = mp.get(i.getName());
            mp.remove(i.getName());
            cnt--;
            if (cnt > 0)
                mp.put (i.getName(), cnt);
        }
        if (mp.containsKey(cardName) && selectedHero.getDeck().size() < InitialLoading.getInstance().getHeroDeckCapacity()) {
            for (Card i: cards) {
                if (i.getName().equals(cardName) && !selectedHero.getDeck().contains(i)) {
                    selectedHero.getDeck().add(i);
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public boolean removeFromDeck(String cardName) {
        HashMap<String, Integer> mp = new HashMap<>();
        for (Card i: selectedHero.getDeck()) {
            if (mp.containsKey(i.getName())) {
                int c = mp.get(i.getName());
                mp.remove(i.getName());
                mp.put(i.getName(), c + 1);
            }
            else {
                mp.put(i.getName(), 1);
            }
        }
        if (mp.containsKey(cardName)) {
            for (int i = cards.size() - 1; i >= 0; i--) {
                if (cards.get(i).getName().equals(cardName)) {
                    selectedHero.getDeck().remove(cards.get(i));
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public int howMuchCanBuy (String cardName) {
        HashMap<String, Integer> mp = new HashMap<>();
        for (String i: InitialLoading.getInstance().getCards()) {
            Card nowCard = CardReader.createCard(i, this.id);
            if (nowCard.getCardClass().equals("neutral")) {
                mp.put(i, (int)InitialLoading.getInstance().getMaxPerCard());
            }
            else {
                for (Hero j: heros) {
                    if (j.getName().equals(nowCard.getCardClass())) {
                        mp.put (i, (int)InitialLoading.getInstance().getMaxPerCard());
                        break;
                    }
                }
            }
        }
        for (Card i: cards) {
            int c = mp.get(i.getName());
            mp.remove(i.getName());
            if (c > 1)
                mp.put(i.getName(), c - 1);
        }
        long cntCard = mp.getOrDefault(cardName, 0);
        if (cntCard > 0) {
            long price = CardReader.createCard(cardName, "A").getCost();
            int ret = (int)(this.getCoins() / price);
            if (ret > cntCard)
                ret = (int)cntCard;
            return ret;
        }
        return 0;
    }

    public int howMuchCanSell (String cardName) {
        int c = 0;
        for (Card i: cards)
            if (i.getName().equals(cardName))
                c++; //C++ is everyWhere :)
        int mx = 0;
        for (Hero i: heros) {
            int cnt2 = 0;
            for (Card j: i.getDeck()) {
                if (j.getName().equals(cardName))
                    cnt2++;
            }
            if (cnt2 > mx)
                mx = cnt2;
        }
        c -= mx;
        return c;
    }

    public void buyCard (String cardName, int cnt) {
        for (int i = 0; i < cnt; i++) {
            Card nowCard = CardReader.createCard(cardName, this.id);
            this.coins -= nowCard.getCost();
            cards.add(nowCard);
        }
    }

    public void sellCard (String cardName, int cnt) {
        for (int i = 0; i < cnt; i++) {
            for (int j = cards.size() - 1; j >= 0; j--) {
                if (cards.get(j).getName().equals(cardName)) {
                    this.coins += cards.get(j).getCost() / 2;
                    cards.remove(cards.get(j));
                    break;
                }
            }
        }
    }

    public void canSellCards () { // DELETE THIS
        HashSet<String> st = new HashSet<>();
        for (Card i: cards)
            st.add (i.getName());
        System.out.println(ConsoleColors.YELLOW + "You can sell these cards:" + ConsoleColors.RESET);
        Map <String, Integer> mp = new HashMap<>();
        for (String i: st) {
            int c = this.howMuchCanSell(i);
            if (c > 0)
                mp.put(i, c);
        }
//        try {
//            //Writer.showTable(mp);
//        } catch (FileNotFoundException ignored) {
//
//        }
    }

    public void canBuyCards () { // DELETE THIS
        System.out.println(ConsoleColors.YELLOW + "You can buy these cards:" + ConsoleColors.RESET);
        Map <String, Integer> mp = new HashMap<>();
        for (String i: InitialLoading.getInstance().getCards()) {
            int c = this.howMuchCanBuy(i);
            if (c > 0)
                mp.put(i, c);
        }
//        try {
//            //Writer.showTable(mp);
//        } catch (FileNotFoundException ignored) {
//
//        }
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public long getCoins() {
        return coins;
    }

    public String getUsername() {
        return username;
    }

    public List<Card> getCards() {
        return cards;
    }

    public List<Hero> getHeros() {
        return heros;
    }

    public Hero getSelectedHero() {
        return selectedHero;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCoins(long coins) {
        this.coins = coins;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void setHeros(List<Hero> heros) {
        this.heros = heros;
    }

    public void setSelectedHero(Hero selectedHero) {
        this.selectedHero = selectedHero;
    }
}