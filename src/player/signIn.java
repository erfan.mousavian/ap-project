package player;

import engine.Engine;
import exeptions.MyException;
import logger.LogWriter;
import main.Blackbox;
import environments.Menu;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;

public class signIn {
    private static signIn singleton = null;

    public static signIn getInstance() {
        if (singleton == null)
            singleton = new signIn();
        return singleton;
    }

    public void logIn (String username, String password) throws MyException {
        if (password == null || password.length() == 0)
            throw new MyException("Your username/password is incorrect.");

        password = Blackbox.getInstance().getHash(password);

        File f = new File ("src"+File.separator+"player"+File.separator+"data"+File.separator+username+".json");
        if (f.exists()) {
            JSONParser parser = new JSONParser();
            JSONObject obj = null;
            try {
                obj = (JSONObject) parser.parse (new FileReader(f));
            } catch (Exception e) {
                LogWriter.programLog("error in parse in login", username + " " + f.getPath() + " " + e.getMessage());
                throw new MyException("Something went wrong. please try again.");
            }

            if (obj.get("password").equals(password)) {
                Engine.getInstance().addPlayer(new Player(username));
                LogWriter.appendLog(Engine.getInstance().getPlayer(0),"login " + Blackbox.getInstance().getCurrentTime());
                Menu.getInstance().show();
            }
            else
                throw new MyException("Your username/password is incorrect.");
        }
        else
            throw new MyException("Your username/password is incorrect.");
    }
}
