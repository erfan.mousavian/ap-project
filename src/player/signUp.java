package player;

import elements.Card;
import elements.CardReader;
import exeptions.MyException;
import logger.LogWriter;
import main.Blackbox;
import main.InitialLoading;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

public class signUp {
    private static signUp singleton = null;

    public static signUp getInstance() {
        if (singleton == null)
            singleton = new signUp();
        return singleton;
    }

    public void createUser(String username, String password) throws MyException {
        File f = new File ("src"+File.separator+"player"+File.separator+"data"+File.separator+username+".json");

        if (!isValid(username) || !isValid(password))
            throw new MyException("Invalid username/password! A-Z, a-z, 0-9, !$%^&");


        boolean created = false;
        try {
            created = f.createNewFile();
        } catch (Exception ignored) {}

        if (created) {
            // Hash password
            password = Blackbox.getInstance().getHash(password);

            // select ID
            String ownerID = Blackbox.getInstance().getRandomString(8);

            // select Hero
            String playerHero = InitialLoading.getInstance().getRandomHero();

            // Select Cards
            ArrayList<Card> playerCards = new ArrayList<>();
            ArrayList<Card> allCards = new ArrayList<>();
            for (String i: InitialLoading.getInstance().getCards()) {
                Card a = CardReader.createCard(i, ownerID);
                if (a.getCardClass().equals(playerHero) || a.getCardClass().equals("neutral")) {
                    allCards.add (CardReader.createCard(i, ownerID));
                    allCards.add (CardReader.createCard(i, ownerID));
                }
            }
            Collections.shuffle(allCards);
            int cntNeutral = 0, cntHero = 0;
            for (Card i: allCards) {
                if (i.getCardClass().equals(playerHero)) {
                    if (cntHero < 1) {
                        cntHero++;
                        playerCards.add (i);
                    }
                }
                else {
                    if (cntNeutral < 9) {
                        cntNeutral++;
                        playerCards.add (i);
                    }
                }
            }
            // Make Hero
            List<JSONObject> heros = new ArrayList<>();
            heros.add (CardReader.createHero(playerHero, playerCards).toJsonObject());

            //Make playerJsonCards
            ArrayList<String> playerJsonCards = new ArrayList<>();
            for (Card i: playerCards)
                playerJsonCards.add(i.getName());

            JSONObject player = new JSONObject();
            player.put ("id", ownerID);
            player.put ("username", username);
            player.put ("password", password);
            player.put ("coins", InitialLoading.getInstance().getPlayerCoins());
            player.put ("heros", heros);
            player.put ("selectedHero", playerHero);
            player.put ("cards", playerJsonCards);
            try {
                FileWriter fw = new FileWriter(f);
                fw.write(player.toJSONString());
                fw.flush();
                fw.close();
                LogWriter.createUser(username, ownerID, password);
            } catch (Exception ignored) {}
        }
        else
            throw new MyException("An account already exists with this username.");
    }

    boolean isValid (String s) {
        String pat = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz!$%^&";
        if (s == null || s.length() == 0)
            return false;
        for (int i = 0; i < s.length(); i++) {
            String z = "";
            z += s.charAt(i);
            if (!pat.contains(z))
                return false;
        }
        return true;
    }
}
